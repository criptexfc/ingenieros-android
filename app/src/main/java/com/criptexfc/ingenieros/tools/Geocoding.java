package com.criptexfc.ingenieros.tools;

import android.content.Context;
import android.location.Address;
import android.location.Location;

import com.criptexfc.ingenieros.DTO.Estructuras;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.SmartLocation;

/**
 * Created by G1L21088 on 23/09/2017.
 */

@SuppressWarnings({"unused", "WeakerAccess"})
public class Geocoding {

    public interface Respuesta {
        void onResult(Estructuras.Direccion result);
    }

    public static void getDireccion(Context context, LatLng latLng, Respuesta respuesta) {
        getDireccion(context, latLng.latitude, latLng.longitude, respuesta);
    }

    public static void getDireccion(Context context, double latitude, double longitude, Respuesta respuesta) {
        Location targetLocation = new Location("");
        targetLocation.setLatitude(latitude);
        targetLocation.setLongitude(longitude);
        getDireccion(context, targetLocation, respuesta);
    }

    public static void getDireccion(Context context, Location location, final Respuesta respuesta) {
        SmartLocation.with(context).geocoding()
                .reverse(location, new OnReverseGeocodingListener() {
                    @Override
                    public void onAddressResolved(Location location, List<Address> list) {
                        if (list.size() > 0)
                            respuesta.onResult(new Estructuras.Direccion(
                                    list.get(0).getThoroughfare(),
                                    list.get(0).getFeatureName(),
                                    list.get(0).getLocality(),
                                    list.get(0).getPostalCode()
                            ));
                        else
                            respuesta.onResult(new Estructuras.Direccion());
                    }
                });
    }
}
