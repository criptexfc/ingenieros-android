package com.criptexfc.ingenieros.network

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

/**
 * @author Gorro
 * @since 23/09/17
 */

interface Endpoints {

    @GET("getMensaje/{randomId}")
    fun getMensaje(@Path("randomId") lat: String): Call<ResponseListInfo>

    @GET("getInfo")
    fun getLista(): Call<ResponseListInfo>

    @GET("getCercanos/{lat}/{lng}")
    fun getCercanos(@Path("lat") lat: String, @Path("lng") lng: String): Call<ResponseListInfo>

    @Multipart
    @POST("addMensaje")
    fun addMessage(@Part image: MultipartBody.Part,
                   @Part imageElement: MultipartBody.Part,
                   @Part("contacto") contacto: RequestBody,
                   @Part("telefono") telefono: RequestBody,
                   @Part("observaciones") observaciones: RequestBody,
                   @Part("longitud") longitud: RequestBody,
                   @Part("latitud") latitud: RequestBody,
                   @Part("correo") correo: RequestBody,
                   @Part("calle") calle: RequestBody,
                   @Part("colonia") colonia: RequestBody,
                   @Part("municipio") municipio: RequestBody,
                   @Part("cp") cp: RequestBody,
                   @Part("tipo") tipo: RequestBody,
                   @Part("niveles") niveles: RequestBody,
                   @Part("residentes") residentes: RequestBody,
                   @Part("afectaciones") afectaciones: RequestBody,
                   @Part("ubicacion") ubicacion: RequestBody,
                   @Part("evacuado") evacuado: RequestBody
    ): Call<ResponseAddMsg>

}