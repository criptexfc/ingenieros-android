package com.criptexfc.ingenieros.network

import com.google.gson.annotations.SerializedName

data class ResponseListInfo(

        @field:SerializedName("success")
        val success: Int? = null,

        @field:SerializedName("extra")
        val extra: List<ExtraItem?>? = null
)

data class ExtraItem(

        @field:SerializedName("fecha")
        val fecha: String? = null,

        @field:SerializedName("contacto")
        val contacto: String? = null,

        @field:SerializedName("latitud")
        val latitud: String? = null,

        @field:SerializedName("longitud")
        val longitud: String? = null,

        @field:SerializedName("estado")
        val estado: String? = null,

        @field:SerializedName("foto")
        val foto: String? = null,

        @field:SerializedName("observaciones")
        val observaciones: String? = null,

        @field:SerializedName("id")
        val id: String? = null,

        @field:SerializedName("telefono")
        val telefono: String? = null,

        @field:SerializedName("status")
        val status: String? = null,

        @field:SerializedName("distancia")
        val distancia: String? = null
)