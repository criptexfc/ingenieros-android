package com.criptexfc.ingenieros.network

import com.google.gson.annotations.SerializedName

data class ResponseAddMsg(

        @field:SerializedName("success")
        val success: Int? = null,

        @field:SerializedName("extra")
        val extra: String? = null,

        @field:SerializedName("random_id")
        val randomId: String? = null
)