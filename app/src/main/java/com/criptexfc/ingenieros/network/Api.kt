package com.criptexfc.ingenieros.network

import android.content.Context
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import com.criptexfc.ingenieros.BuildConfig
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author Gorro
 * @since 23/09/17
 */
object Api {

    val baseURL = "http://criptexfc.com/tlalolin/admin/app/"
    lateinit var retrofit: Retrofit
    lateinit var endpoints: Endpoints
    lateinit var context: Context

    @JvmStatic
    fun initApi(ctx: Context) {
        context = ctx
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
        val builder = OkHttpClient.Builder()
        builder.interceptors().add(loggingInterceptor)
        val client = builder.build()

        retrofit = Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

        endpoints = retrofit.create(Endpoints::class.java)
    }

    @JvmStatic
    fun sendReport(imgFachada: ByteArray,
                   imgElements: ByteArray,
                   contacto: String,
                   phone: String,
                   observations: String,
                   lng: String,
                   lat: String,
                   mail: String,
                   street: String,
                   colony: String,
                   delMun: String,
                   cp: String,
                   type: String,
                   levels: String,
                   residents: String,
                   afectaciones: List<CharSequence>,
                   location: String,
                   evacuated: String) {

        Log.e("ELEMENTS", "${afectaciones.joinToString()}")

        val fileImgFachada = RequestBody.create(MediaType.parse("image/jpeg"), imgFachada)
        val fileImgElement = RequestBody.create(MediaType.parse("image/jpeg"), imgElements)
        val bodyMultipartF = MultipartBody.Part.createFormData("file", "file.jpg", fileImgFachada)
        val bodyMultipartE = MultipartBody.Part.createFormData("file_elementos", "file.jpg", fileImgElement)

        val rqBodyCont = RequestBody.create(MediaType.parse("text/plain"), contacto)
        val rqBodyPho = RequestBody.create(MediaType.parse("text/plain"), phone)
        val rqBodyObs = RequestBody.create(MediaType.parse("text/plain"), observations)
        val rqBodyLat = RequestBody.create(MediaType.parse("text/plain"), lat)
        val rqBodyLng = RequestBody.create(MediaType.parse("text/plain"), lng)

        val rqBodyMail = RequestBody.create(MediaType.parse("text/plain"), mail)
        val rqBodyStreet = RequestBody.create(MediaType.parse("text/plain"), street)
        val rqBodyColony = RequestBody.create(MediaType.parse("text/plain"), colony)
        val rqBodyDelMun = RequestBody.create(MediaType.parse("text/plain"), delMun)
        val rqBodyCP = RequestBody.create(MediaType.parse("text/plain"), cp)
        val rqBodyType = RequestBody.create(MediaType.parse("text/plain"), type)
        val rqBodyLevels = RequestBody.create(MediaType.parse("text/plain"), levels)
        val rqBodyResidents = RequestBody.create(MediaType.parse("text/plain"), residents)
        val rqBodyAfectaciones = RequestBody.create(MediaType.parse("text/plain"), afectaciones.joinToString(prefix = "[", postfix = "]"))
        val rqBodyLocation = RequestBody.create(MediaType.parse("text/plain"), location)
        val rqBodyEvacuated = RequestBody.create(MediaType.parse("text/plain"), evacuated)

        val call = endpoints.addMessage(bodyMultipartF,
                bodyMultipartE,
                rqBodyCont,
                rqBodyPho,
                rqBodyObs,
                rqBodyLng,
                rqBodyLat,
                rqBodyMail,
                rqBodyStreet,
                rqBodyColony,
                rqBodyDelMun,
                rqBodyCP,
                rqBodyType,
                rqBodyLevels,
                rqBodyResidents,
                rqBodyAfectaciones,
                rqBodyLocation,
                rqBodyEvacuated)

        call.enqueue(object : Callback<ResponseAddMsg> {
            override fun onResponse(call: Call<ResponseAddMsg>?, response: Response<ResponseAddMsg>?) {
                if (response?.isSuccessful == true) {
                    if (response.body()?.success == 1) {
                        val dialog = AlertDialog.Builder(context)
                                .setTitle(response.body()?.extra)
                                .setMessage("Tu reporte ha sido enviado, guarda el siguiente codigo de reporte para poder consultar su estado mas adelante\n\n${response.body()?.randomId}")
                                .setPositiveButton("Ok", { dialogInterface, _ ->
                                    dialogInterface.dismiss()
                                })
                                .create()
                        dialog.show()
                    } else {
                        Toast.makeText(context, response.body()?.extra ?: "Algo salio mal, intenta de nuevo", Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(context, "Algo salio mal, intenta de nuevo", Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<ResponseAddMsg>?, t: Throwable?) {
                Toast.makeText(context, t?.message ?: "Algo salio mal, intenta de nuevo", Toast.LENGTH_LONG).show()
            }
        })

    }

    @JvmStatic
    fun reciveData(rsp: OnResponseListener) {
        val call = endpoints.getLista()
        call.enqueue(object : Callback<ResponseListInfo> {
            override fun onResponse(call: Call<ResponseListInfo>?, response: Response<ResponseListInfo>?) {
                if (response?.code() == 200) {
                    rsp.success(response.body())
                } else {
                    rsp.error(Throwable("Algo salio mal. Intenta nuevamente"))
                }
            }

            override fun onFailure(call: Call<ResponseListInfo>?, t: Throwable?) {
                rsp.error(t ?: Throwable("Algo salio mal. Intenta nuevamente"))
            }

        })
    }

    @JvmStatic
    fun reciveInfo(param: String, rsp: OnResponseListener) {
        val call = endpoints.getMensaje(param)
        call.enqueue(object : Callback<ResponseListInfo> {
            override fun onResponse(call: Call<ResponseListInfo>?, response: Response<ResponseListInfo>?) {
                if (response?.code() == 200) {
                    rsp.success(response.body())
                } else {
                    rsp.error(Throwable("Algo salio mal. Intenta nuevamente"))
                }
            }

            override fun onFailure(call: Call<ResponseListInfo>?, t: Throwable?) {
                rsp.error(t ?: Throwable("Algo salio mal. Intenta nuevamente"))
            }
        })
    }

}