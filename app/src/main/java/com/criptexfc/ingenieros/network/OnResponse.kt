package com.criptexfc.ingenieros.network

import android.util.Log

/**
 * @author Gorro
 * @since 23/09/17
 */

interface OnResponse {
    fun success(response: ResponseListInfo?)
    fun successMsg(response: ResponseAddMsg?)
    fun error(t: Throwable)
}

open class OnResponseListener : OnResponse {
    override fun success(response: ResponseListInfo?) {
        Log.i("Success", "success response")
    }

    override fun successMsg(response: ResponseAddMsg?) {
        Log.i("Success", "success response Msg")
    }

    override fun error(t: Throwable) {
        Log.i("Error", "error api")
    }

}