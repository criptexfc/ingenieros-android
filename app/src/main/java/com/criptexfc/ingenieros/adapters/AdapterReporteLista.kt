package com.criptexfc.ingenieros.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.criptexfc.ingenieros.R
import com.criptexfc.ingenieros.network.ExtraItem
import com.criptexfc.ingenieros.utils.loadImage
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.item_lista_reporte.view.*
import java.util.*

/**
 * @author Gorro
 * @since 23/09/17
 */
class AdapterReporteLista(val data: List<ExtraItem>) : RecyclerView.Adapter<AdapterReporteLista.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_lista_reporte, parent, false))

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bindView(data[position])
    }

    override fun getItemCount(): Int = data.size


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindView(itemExtra: ExtraItem) {
            with(itemExtra) {
                itemView.txtItemContacto.text = contacto
                itemView.txtItemNum.text = telefono
                itemView.txtItemObservaciones.text = observaciones
                itemView.imgItemLista.loadImage(foto)
                val distance: Double = try {
                    distancia?.toDouble() ?: -1.0
                } catch (e: Exception) {
                    -1.0
                }
                val status = try {
                    estado?.toInt()
                } catch (e: NumberFormatException) {
                    -1
                }
                val latlng = try {
                    val latD: Double = latitud?.toDouble() ?: 0.0
                    val lngD: Double = longitud?.toDouble() ?: 0.0
                    LatLng(latD, lngD)
                } catch (e: Exception) {
                    LatLng(0.0, 0.0)
                }
                val ctx = itemView.context
                when (status) {
                    1 -> {
                        itemView.txtItemStatus.setText(R.string.frg_lista_pending)
                        itemView.txtItemStatus.setTextColor(ctx.resources.getColor(android.R.color.holo_blue_dark))
                    }
                    2 -> {
                        itemView.txtItemStatus.setText(R.string.frg_lista_checked)
                        itemView.txtItemStatus.setTextColor(ctx.resources.getColor(android.R.color.holo_green_dark))
                    }
                    3 -> {
                        itemView.txtItemStatus.setText(R.string.frg_lista_need_physic_check)
                        itemView.txtItemStatus.setTextColor(ctx.resources.getColor(android.R.color.holo_orange_dark))
                    }
                    4 -> {
                        itemView.txtItemStatus.setText(R.string.frg_lista_urgent_revision)
                        itemView.txtItemStatus.setTextColor(ctx.resources.getColor(android.R.color.holo_red_dark))
                    }
                }

                if (distance != -1.0) {
                    itemView.containerDistance.visibility = View.VISIBLE
                    itemView.txtItemDistancia.text = String.format(ctx.getString(R.string.frg_lista_distance), String.format(Locale.getDefault(), "%.2f", distance))
                }

                itemView.mapViewItemLista.onCreate(null)
                itemView.mapViewItemLista.getMapAsync {
                    val cameraPosition = CameraPosition.Builder()
                            .target(latlng)
                            .zoom(16f)
                            .build()
                    it.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                    it.uiSettings.setAllGesturesEnabled(false)
                    it.uiSettings.isMyLocationButtonEnabled = false
                    it.addMarker(MarkerOptions().position(latlng))
                }
            }
        }
    }

}