package com.criptexfc.ingenieros.utils

import android.graphics.Bitmap
import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.criptexfc.ingenieros.R
import java.io.ByteArrayOutputStream

/**
 * @author Gorro
 * @since 23/09/17
 */


fun ImageView.loadImage(urlNameImage: String?) {
    val baseURlImages = ""
    Glide.with(this.context)
            .load(baseURlImages + urlNameImage)
            .asBitmap()
            .placeholder(R.mipmap.ic_launcher)
            .error(R.mipmap.ic_launcher)
            .into(this)
}

fun getFileDataBm(bmp: Bitmap): ByteArray {
    val bitmap = Bitmap.createScaledBitmap(bmp, 800, 800, true)
    val byteArrayOS = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 25, byteArrayOS)
    return byteArrayOS.toByteArray()
}

fun printList(data: List<Any>) {
    Log.e("ELEMENTS", "${data.toString()}")
    Log.e("ELEMENTS", "${data.joinToString()}")
    val elem = data.joinToString(prefix = "[", postfix = "]", transform = {
        "'$it'"
    })

    Log.e("ELEMENTS", elem)

}