package com.criptexfc.ingenieros;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.criptexfc.ingenieros.DTO.Estructuras;
import com.criptexfc.ingenieros.network.Api;
import com.criptexfc.ingenieros.tools.Geocoding;
import com.criptexfc.ingenieros.utils.UtilsKt;
import com.flurgle.camerakit.CameraListener;
import com.flurgle.camerakit.CameraView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.DialogOnAnyDeniedMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.thomashaertel.widget.MultiSpinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class FragmentReporte extends Fragment implements OnMapReadyCallback {

    private static final String TAG = "FRAGMENTREPORTE";

    boolean arGranted = false;
    @BindView(R.id.edtxReporteContacto)
    EditText edtxReporteContacto;
    @BindView(R.id.edtxReportePhone)
    EditText edtxReportePhone;
    @BindView(R.id.edtxReporteMail)
    EditText edtxReporteMail;
    @BindView(R.id.edtxReporteStreet)
    EditText edtxReporteStreet;
    @BindView(R.id.edtxReporteExtNumber)
    EditText edtxReporteExtNumber;
    @BindView(R.id.edtxReporteCol)
    EditText edtxReporteCol;
    @BindView(R.id.spinnerReporteDelMun)
    Spinner spinnerReporteDelMun;
    @BindView(R.id.edtxReporteCP)
    EditText edtxReporteCP;
    @BindView(R.id.spinnerReporteInmuebleType)
    Spinner spinnerReporteInmuebleType;
    @BindView(R.id.spinnerReporteMainDamage)
    MultiSpinner spinnerReporteMainDamage;
    @BindView(R.id.spinnerReporteDammagePlace)
    Spinner spinnerReporteDammagePlace;
    @BindView(R.id.edtxReporteFloors)
    EditText edtxReporteFloors;
    @BindView(R.id.edtxReportePersonsLive)
    EditText edtxReportePersonsLive;
    @BindView(R.id.spinnerReporteEvacuated)
    Spinner spinnerReporteEvacuated;
    @BindView(R.id.edtxReporteObservations)
    EditText edtxReporteObservations;
    @BindView(R.id.cameraFachada)
    CameraView cameraFachada;
    @BindView(R.id.capturaFachada)
    Button capturaFachada;
    @BindView(R.id.cameraDamageElements)
    CameraView cameraDamageElements;
    @BindView(R.id.capturaDamageElements)
    Button capturaDamageElements;
    @BindView(R.id.botonSendReporte)
    Button botonSendReporte;
    SupportMapFragment mapaReporte;
    Unbinder unbinder;
    private LatLng ubicacion;
    private Bitmap imagen;
    private Bitmap imagenDamageElements;
    private List<String> listDamageTypes;
    private List<String> listDamageTypesReduce = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reporte, container, false);

        unbinder = ButterKnife.bind(this, view);

        mapaReporte = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapaReporte);

        listDamageTypesReduce.add("derrumbeTotal");
        listDamageTypesReduce.add("derrumbeParcial");
        listDamageTypesReduce.add("grietasMuros");
        listDamageTypesReduce.add("grietasTrabes");
        listDamageTypesReduce.add("asentamiento");
        listDamageTypesReduce.add("danoVidrios");

        checkPermissionsAgain();

        ArrayAdapter<CharSequence> adapterDelMun = ArrayAdapter.createFromResource(getContext(), R.array.frg_reporte_n_delegation_list, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapterInmueble = ArrayAdapter.createFromResource(getContext(), R.array.frg_reporte_n_property_list, android.R.layout.simple_spinner_item);
        final ArrayAdapter<CharSequence> adapterDamagePlace = ArrayAdapter.createFromResource(getContext(), R.array.frg_reporte_n_damage_place_list, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapterEvacuated = ArrayAdapter.createFromResource(getContext(), R.array.frg_reporte_n_damage_evacuated_list, android.R.layout.simple_spinner_item);

        adapterDelMun.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        adapterInmueble.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        adapterDamagePlace.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        adapterEvacuated.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        final ArrayAdapter<CharSequence> adapterMainDamage = ArrayAdapter.createFromResource(getContext(), R.array.frg_reporte_n_damage_list, android.R.layout.simple_spinner_item);

        spinnerReporteDelMun.setAdapter(adapterDelMun);
        spinnerReporteInmuebleType.setAdapter(adapterInmueble);
        spinnerReporteDammagePlace.setAdapter(adapterDamagePlace);
        spinnerReporteEvacuated.setAdapter(adapterEvacuated);

        spinnerReporteMainDamage.setAdapter(adapterMainDamage, false, new MultiSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {
                listDamageTypes = new ArrayList<>();
                for (int i = 0; i < selected.length; i++) {
                    boolean itemSelected = selected[i];
                    if (itemSelected) {
                        listDamageTypes.add(listDamageTypesReduce.get(i));
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        capturaFachada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arGranted) {
                    try {
                        cameraFachada.captureImage();
                    } catch (Exception e) {
                        cameraDamageElements.stop();
                        cameraFachada.start();
                    }
                } else {
                    checkPermissionsAgain();
                }

            }
        });

        capturaDamageElements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (arGranted) {
                    try {
                        cameraDamageElements.captureImage();
                    } catch (Exception e) {
                        cameraFachada.stop();
                        cameraDamageElements.start();
                    }
                } else {
                    checkPermissionsAgain();
                }
            }
        });

        botonSendReporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imagen != null && imagenDamageElements != null)
                    sendData();
                else
                    Toast.makeText(getContext(), R.string.frg_lista_not_image_taken, Toast.LENGTH_SHORT).show();
            }
        });

        cameraFachada.setCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] jpeg) {
                super.onPictureTaken(jpeg);
                imagen = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length);
            }
        });

        cameraDamageElements.setCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] jpeg) {
                super.onPictureTaken(jpeg);
                imagenDamageElements = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length);
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        cameraFachada.stop();
        cameraDamageElements.stop();
    }

    private void checkPermissionsAgain() {
        Dexter.withActivity(getActivity()).withPermissions(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    //cameraFachada.start();
                    //cameraDamageElements.start();
                    mapaReporte.getMapAsync(FragmentReporte.this);
                    arGranted = true;
                } else {
                    Toast.makeText(getActivity(), R.string.frg_lista_permission_message_toast, Toast.LENGTH_SHORT).show();
                    MultiplePermissionsListener listener = DialogOnAnyDeniedMultiplePermissionsListener.Builder
                            .withContext(getContext())
                            .withTitle(R.string.frg_lista_title_alert_permissions)
                            .withMessage(R.string.frg_lista_message_alert_permissions)
                            .withButtonText("Ok")
                            .build();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        final SmartLocation smartLocation = SmartLocation.with(getContext());
        if (smartLocation.location().state().isAnyProviderAvailable())
            smartLocation.location().oneFix().start(new OnLocationUpdatedListener() {
                @Override
                public void onLocationUpdated(Location location) {
                    ubicacion = new LatLng(location.getLatitude(), location.getLongitude());
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(location.getLatitude(), location.getLongitude()))
                            .zoom(18)
                            .build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    googleMap.getUiSettings().setAllGesturesEnabled(true);
                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    googleMap.setMyLocationEnabled(true);
                    googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                        @Override
                        public void onMapClick(LatLng latLng) {
                            googleMap.clear();
                            googleMap.addMarker(new MarkerOptions().position(latLng));
                            ubicacion = new LatLng(latLng.latitude, latLng.longitude);
                            Geocoding.getDireccion(getContext(), latLng, new Geocoding.Respuesta() {
                                @Override
                                public void onResult(Estructuras.Direccion result) {
                                    edtxReporteStreet.setText(result.getCalle());
                                    edtxReporteExtNumber.setText(result.getNumero());
                                    edtxReporteCP.setText(result.getCodigo_postal());
                                }
                            });
                        }
                    });
                }
            });
    }

    private void sendData() {
        if (validForm()) {
            if (imagen != null && imagenDamageElements != null) {
                UtilsKt.getFileDataBm(imagen);
                UtilsKt.getFileDataBm(imagenDamageElements);
                Api.sendReport(UtilsKt.getFileDataBm(imagen),
                        UtilsKt.getFileDataBm(imagenDamageElements),
                        edtxReporteContacto.getText().toString(),
                        edtxReportePhone.getText().toString(),
                        edtxReporteObservations.getText().toString(),
                        String.valueOf(ubicacion.longitude),
                        String.valueOf(ubicacion.latitude),
                        edtxReporteMail.getText().toString(),
                        edtxReporteStreet.getText().toString(),
                        edtxReporteCol.getText().toString(),
                        spinnerReporteDelMun.getSelectedItem().toString(),
                        edtxReporteCP.getText().toString(),
                        spinnerReporteInmuebleType.getSelectedItem().toString(),
                        edtxReporteFloors.getText().toString(),
                        edtxReportePersonsLive.getText().toString(),
                        listDamageTypesReduce,
                        spinnerReporteDammagePlace.getSelectedItem().toString(),
                        spinnerReporteEvacuated.getSelectedItem().toString());
            } else {
                Toast.makeText(getActivity(), "Por favor toma las fotos correspondientes para un reporte optimo", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getActivity(), R.string.frg_lista_message_need_data, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validForm() {
        boolean isValid = true;
        if (edtxReporteContacto.getText().toString().isEmpty()) {
            isValid = false;
        } else if (edtxReportePhone.getText().toString().isEmpty()) {
            isValid = false;
        } else if (edtxReporteCol.getText().toString().isEmpty()) {
            isValid = false;
        } else if (edtxReporteCP.getText().toString().isEmpty()) {
            isValid = false;
        } else if (edtxReporteExtNumber.getText().toString().isEmpty()) {
            isValid = false;
        } else if (edtxReporteFloors.getText().toString().isEmpty()) {
            isValid = false;
        } else if (edtxReporteMail.getText().toString().isEmpty()) {
            isValid = false;
        } else if (edtxReporteObservations.getText().toString().isEmpty()) {
            isValid = false;
        } else if (edtxReportePersonsLive.getText().toString().isEmpty()) {
            isValid = false;
        } else if (edtxReporteStreet.getText().toString().isEmpty()) {
            isValid = false;
        } else if (listDamageTypes.size() == 0) {
            isValid = false;
        }
        return isValid;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
