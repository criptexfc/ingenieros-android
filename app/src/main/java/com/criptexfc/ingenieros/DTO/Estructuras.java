package com.criptexfc.ingenieros.DTO;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonObject;

import java.io.Serializable;

/**
 * Created by G1L21088 on 20/09/2017.
 */

public class Estructuras {

    public static class Evento implements Serializable, Parcelable {
        private String contacto;
        private String telefono;
        private String notas;
        private String imagen;
        private LatLng latLng;
        private int estado;
        private int id;
        private double distancia;

        @Override
        public String toString() {
            return "Evento{" +
                    "contacto='" + contacto + '\'' +
                    ", telefono='" + telefono + '\'' +
                    ", notas='" + notas + '\'' +
                    ", imagen='" + imagen + '\'' +
                    ", latLng=" + latLng +
                    ", estado=" + estado +
                    ", id=" + id +
                    ", distancia=" + distancia +
                    '}';
        }

        public String getContacto() {
            return contacto;
        }

        public String getTelefono() {
            return telefono;
        }

        public String getNotas() {
            return notas;
        }

        public String getImagen() {
            return imagen;
        }

        public LatLng getLatLng() {
            return latLng;
        }

        public int getEstado() {
            return estado;
        }

        public int getId() {
            return id;
        }

        public double getDistancia() {
            return distancia;
        }

        public Evento(JsonObject data) {
            this.contacto = (data.get("contacto") == null || data.get("contacto").isJsonNull()) ? "" : data.get("contacto").getAsString();
            this.telefono = (data.get("telefono") == null || data.get("telefono").isJsonNull()) ? "" : data.get("telefono").getAsString();
            this.notas = (data.get("observaciones") == null || data.get("observaciones").isJsonNull()) ? "" : data.get("observaciones").getAsString();
            this.imagen = "http://criptexfc.com/tlalolin/img/" + ((data.get("foto") == null || data.get("foto").isJsonNull()) ? "" : data.get("foto").getAsString());
            this.latLng = new LatLng(
                    Double.parseDouble((data.get("latitud") == null || data.get("latitud").isJsonNull()) ? "0.0" : data.get("latitud").getAsString()),
                    Double.parseDouble((data.get("longitud") == null || data.get("longitud").isJsonNull()) ? "0.0" : data.get("longitud").getAsString())
            );
            this.estado = Integer.parseInt((data.get("estado") == null || data.get("estado").isJsonNull()) ? "-1" : data.get("estado").getAsString());
            this.id = Integer.parseInt((data.get("id") == null || data.get("id").isJsonNull()) ? "-1" : data.get("id").getAsString());
            this.distancia = Double.parseDouble((data.get("distancia") == null || data.get("distancia").isJsonNull()) ? "-1.0" : data.get("distancia").getAsString());
        }

        public Evento() {
            this.contacto = "";
            this.telefono = "";
            this.notas = "";
            this.imagen = "";
            this.latLng = new LatLng(0.0, 0.0);
            this.estado = -1;
            this.id = -1;
            this.distancia = -1.0;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.contacto);
            dest.writeString(this.telefono);
            dest.writeString(this.notas);
            dest.writeString(this.imagen);
            dest.writeParcelable(this.latLng, flags);
            dest.writeInt(this.estado);
            dest.writeInt(this.id);
            dest.writeDouble(this.distancia);
        }

        protected Evento(Parcel in) {
            this.contacto = in.readString();
            this.telefono = in.readString();
            this.notas = in.readString();
            this.imagen = in.readString();
            this.latLng = in.readParcelable(LatLng.class.getClassLoader());
            this.estado = in.readInt();
            this.id = in.readInt();
            this.distancia = in.readDouble();
        }

        public static final Creator<Evento> CREATOR = new Creator<Evento>() {
            @Override
            public Evento createFromParcel(Parcel source) {
                return new Evento(source);
            }

            @Override
            public Evento[] newArray(int size) {
                return new Evento[size];
            }
        };
    }

    public static class Direccion {
        private String calle;
        private String numero;
        private String ciudad;
        private String codigo_postal;

        public Direccion() {
            this.calle = "";
            this.numero = "";
            this.ciudad = "";
            this.codigo_postal = "";
        }

        public Direccion(String calle, String numero, String colonia, String codigo_postal) {
            this.calle = calle;
            this.numero = numero;
            this.ciudad = colonia;
            this.codigo_postal = codigo_postal;
        }

        public String getCalle() {
            return calle;
        }

        public String getNumero() {
            return numero;
        }

        public String getCiudad() {
            return ciudad;
        }

        public String getCodigo_postal() {
            return codigo_postal;
        }

        @Override
        public String toString() {
            return "Direccion{" +
                    "calle='" + calle + '\'' +
                    ", numero='" + numero + '\'' +
                    ", ciudad='" + ciudad + '\'' +
                    ", codigo_postal='" + codigo_postal + '\'' +
                    '}';
        }
    }
}
