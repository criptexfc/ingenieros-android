package com.criptexfc.ingenieros;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.criptexfc.ingenieros.DTO.Estructuras;
import com.criptexfc.ingenieros.adapters.AdapterReporteLista;
import com.criptexfc.ingenieros.application.DataCallback;
import com.criptexfc.ingenieros.application.WebServices;
import com.criptexfc.ingenieros.network.Api;
import com.criptexfc.ingenieros.network.ExtraItem;
import com.criptexfc.ingenieros.network.OnResponseListener;
import com.criptexfc.ingenieros.network.ResponseListInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class FragmentLista extends Fragment {

    private static final String TAG = "FRAGMENTLISTA";

    @BindView(R.id.edtxReporteNumero)
    EditText edtxReporteNumero;
    @BindView(R.id.btnBuscar)
    Button btnBuscar;
    @BindView(R.id.rv)
    RecyclerView rv;

    Unbinder unbinder;


    private ArrayList<Estructuras.Evento> listaEventos;
    private int filtro = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lista, container, false);
        unbinder = ButterKnife.bind(this, view);
        rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Api.reciveInfo(edtxReporteNumero.getText().toString(),
                        new OnResponseListener() {
                            @Override
                            public void success(ResponseListInfo response) {
                                super.success(response);
                                if (response.getExtra() != null) {
                                    if (response.getExtra().size() > 0) {
                                        loadList(response.getExtra());
                                    } else {
                                        Toast.makeText(getActivity(), "Aun no hay reportes registrados", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }

                            @Override
                            public void error(@NonNull Throwable t) {
                                super.error(t);
                                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void cargarTodos() {
        Api.reciveData(new OnResponseListener() {
            @Override
            public void success(ResponseListInfo response) {
                super.success(response);
                if (response.getExtra() != null) {
                    if (response.getExtra().size() > 0) {
                        loadList(response.getExtra());
                    } else {
                        Toast.makeText(getActivity(), "Aun no hay reportes registrados", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void error(@NonNull Throwable t) {
                super.error(t);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

//        listaEventos = new ArrayList<>();
//        new WebServices(getContext()).reciveData(new DataCallback.getArrayObjects() {
//            @Override
//            public void onResult(boolean result, ArrayList<Object> extra) {
//                if (result) {
//                    for (Object object : extra)
//                        listaEventos.add((Estructuras.Evento) object);
//                    Log.e("Elementos", listaEventos.toString());
////                    cargarLista();
//                }
//            }
//        });
    }

    private void cargarCercanos() {
        listaEventos = new ArrayList<>();
        final SmartLocation smartLocation = SmartLocation.with(getContext());
        if (smartLocation.location().state().isAnyProviderAvailable())
            smartLocation.location().oneFix().start(new OnLocationUpdatedListener() {
                @Override
                public void onLocationUpdated(Location location) {
                    new WebServices(getContext()).reciveDataLocation(
                            location.getLatitude(),
                            location.getLongitude(),
                            new DataCallback.getArrayObjects() {
                                @Override
                                public void onResult(boolean result, ArrayList<Object> extra) {
                                    if (result) {
                                        for (Object object : extra)
                                            listaEventos.add((Estructuras.Evento) object);
                                        Log.e("Elementos", listaEventos.toString());
//                                        cargarLista();
                                    }
                                }
                            });
                }
            });
    }

    private void loadList(List<ExtraItem> listaReportes) {
        AdapterReporteLista adapterReporteLista;
        if (filtro != 0) {
            List<ExtraItem> pivote = new ArrayList<>();
            for (ExtraItem evento : listaReportes) {
                int estado = Integer.parseInt(evento.getEstado());
                if (estado == filtro) {
                    pivote.add(evento);
                }
            }
            adapterReporteLista = new AdapterReporteLista(pivote);
        } else {
            adapterReporteLista = new AdapterReporteLista(listaReportes);
        }
        rv.setAdapter(adapterReporteLista);
    }

//    private void cargarLista() {
//        if (listaEventos == null)
//            listaEventos = new ArrayList<>();
//        LinearLayoutManager llm = new LinearLayoutManager(getContext());
//        rv.setLayoutManager(llm);
//        RVAdapter adapter;
//        if (filtro != 0) {
//            ArrayList<Estructuras.Evento> pivote = new ArrayList<>();
//            for (Estructuras.Evento evento : listaEventos) {
//                if (evento.getEstado() == filtro) {
//                    pivote.add(evento);
//                }
//            }
//            adapter = new RVAdapter(pivote);
//        } else {
//            adapter = new RVAdapter(listaEventos);
//        }
//        rv.setAdapter(adapter);
//    }


}
