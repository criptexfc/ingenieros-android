package com.criptexfc.ingenieros;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.criptexfc.ingenieros.network.Api;

public class SplashActivity extends AppCompatActivity {

    public static final int _REQUEST_CODE = 500;
    private Button reporte;
    private Button lista;

    private String[] permisos = {
            android.Manifest.permission.INTERNET,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA
    };
    private FragmentManager fragmentManager;
    private FrameLayout flPantalla;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        reporte = (Button) findViewById(R.id.reporte);
        lista = (Button) findViewById(R.id.lista);
        reporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startApp();
            }
        });

        lista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startList();
            }
        });

        Api.initApi(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(permisos, _REQUEST_CODE);
            } else {
                reporte.setEnabled(true);
                lista.setEnabled(true);
            }
        } else {
            reporte.setEnabled(true);
            lista.setEnabled(true);
        }

    }

    private void startApp() {
        fragmentManager = getSupportFragmentManager();
        flPantalla = (FrameLayout) findViewById(R.id.flPantalla);
        fragmentManager.beginTransaction()
                .replace(flPantalla.getId(), new FragmentReporte(), null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    private void startList() {
        fragmentManager = getSupportFragmentManager();
        flPantalla = (FrameLayout) findViewById(R.id.flPantalla);
        fragmentManager.beginTransaction()
                .replace(flPantalla.getId(), new FragmentLista(), null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        reporte.setEnabled(true);
        lista.setEnabled(true);
    }

}
