package com.criptexfc.ingenieros.application;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.criptexfc.ingenieros.DTO.Estructuras;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.criptexfc.ingenieros.application.VolleyMultipartRequest.FileByteProcess.getFileDataFrom;

/**
 * Created by G1L21088 on 20/09/2017.
 */

public class WebServices {

    private static final String _PATH_REPORTE = "http://criptexfc.com/tlalolin/admin/app/addMensaje";
    private static final String _PATH_LISTA = "http://criptexfc.com/tlalolin/admin/app/getInfo";
    private static final String _PATH_LISTA_CERCANOS = "http://criptexfc.com/tlalolin/admin/app/getCercanos/";
    private static final String tag = "cicluz";

    private ProgressDialog progressDialog;
    private Context context;

    public WebServices() {

    }

    public WebServices(Context context) {
        this.context = context;
        try {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Cargando...");
            progressDialog.setCancelable(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param mensaje Cuando en este parametro se le envie un valor nulo se usara el mensaje default
     */
    private void progressStat(String mensaje) {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage("Cargando...");
                progressDialog.setCancelable(false);
            }
            if (!progressDialog.isShowing())
                progressDialog.show();
            else
                progressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void reciveDataLocation(final Double latitud, final Double longitud, final DataCallback.getArrayObjects dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                _PATH_LISTA_CERCANOS + latitud + "/" + longitud,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressStat(null);
                        JsonParser parser = new JsonParser();
                        JsonObject jsonObject = parser.parse(response).getAsJsonObject();
                        switch (jsonObject.get("success").getAsInt()) {
                            case 1:
                                ArrayList<Object> eventos = new ArrayList<>();
                                JsonArray array = jsonObject.get("extra").getAsJsonArray();
                                for (JsonElement element : array) {
                                    eventos.add(new Estructuras.Evento(element.getAsJsonObject()));
                                }
                                dataCallback.onResult(true, eventos);
                                break;
                            default:
                                dataCallback.onResult(false, null);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(tag, error.toString());
                    }
                }
        );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 4, 1));
        AppController.getInstance().addToRequestQueue(stringRequest, tag);
    }

    public void reciveData(final DataCallback.getArrayObjects dataCallback) {
        progressStat(null);
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                _PATH_LISTA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressStat(null);
                        JsonParser parser = new JsonParser();
                        JsonObject jsonObject = parser.parse(response).getAsJsonObject();
                        switch (jsonObject.get("success").getAsInt()) {
                            case 1:
                                ArrayList<Object> eventos = new ArrayList<>();
                                JsonArray array = jsonObject.get("extra").getAsJsonArray();
                                for (JsonElement element : array) {
                                    eventos.add(new Estructuras.Evento(element.getAsJsonObject()));
                                }
                                dataCallback.onResult(true, eventos);
                                break;
                            default:
                                dataCallback.onResult(false, null);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(tag, error.toString());
                    }
                }
        );
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 4, 1));
        AppController.getInstance().addToRequestQueue(stringRequest, tag);
    }

    public void sendData(final Map<String, String> params, final Bitmap imagen, final DataCallback dataCallback) {
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(
                Request.Method.POST,
                _PATH_REPORTE,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        String resultResponse = new String(response.data);
                        JsonParser parser = new JsonParser();
                        JsonObject jsonObject = parser.parse(resultResponse).getAsJsonObject();
                        switch (jsonObject.get("success").getAsInt()) {
                            case 1:
                                dataCallback.onResult(true);
                                break;
                            default:
                                dataCallback.onResult(false);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i(tag, error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() throws AuthFailureError {
                Map<String, DataPart> params = new HashMap<>();
                params.put("file", new DataPart("file.jpg", getFileDataFrom(imagen), "image/jpeg"));
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(4000, 4, 1));
        AppController.getInstance().addToRequestQueue(volleyMultipartRequest, tag);
    }

}
