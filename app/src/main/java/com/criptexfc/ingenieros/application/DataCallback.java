package com.criptexfc.ingenieros.application;

import java.util.ArrayList;

/**
 * Created by G1L21088 on 20/09/2017.
 */

public interface DataCallback {

    void onResult(boolean result);

    interface getArrayObjects {
        void onResult(boolean result, ArrayList<Object> extra);
    }
}
